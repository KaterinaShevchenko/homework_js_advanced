'use strict'

const books = [
    { 
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70 
    }, 
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    }
  ];

const root = document.createElement("div");
root.id = "root"
document.body.prepend(root);

const nameList = document.createElement("ul");
root.append(nameList);

books.forEach((item, index) => {
    const itemNameList = document.createElement("li");
    try{
        if (!item.author) {
            throw new SyntaxError(`No autor ${index}`);
        } 
        if (!item.name) {
            throw new SyntaxError(`No name ${index}`);
        }
        if (!item.price) {
            throw new SyntaxError(`No price ${index}`);
        }
        
        itemNameList.innerHTML = `${index}. 
        author:${item.author}, 
        name:${item.name},
        price:${item.price}`;
        nameList.append(itemNameList);
  
    } catch(error){
        console.log(error.message);
    }  
});


