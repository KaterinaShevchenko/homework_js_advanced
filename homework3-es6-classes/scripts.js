'use strict'

class Emploee {
  constructor(name, age, salary) {
      this.name = name;
      this.age = age;
      this.salary = salary
  }
  set name(name) {
    this.full_name = name;
    }

    get name() {
        return this.full_name;
    }

    set age(age) {
        this.full_age = age;
    }

    get age() {
        return this.full_age;
    }

    set salary(salary) {
        this.required_salary = salary;
    }

    get salary() {
        return this.required_salary;
    }

}

class Programmer extends Emploee {
    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this.lang = lang;
    }
    set salary(salary) {
        this.required_salary = salary * 3;
    }
    get salary() {
        return this.required_salary;
    }
}

const programmer1 = new Programmer("Katya", "21", "5000", ["js, css, html"]);
console.log(programmer1);

const programmer2 = new Programmer("Jools", "26", "5000", ["js, css, html, java"]);
console.log(programmer2);