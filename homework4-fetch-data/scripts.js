'use strict'

const url = 'https://swapi.dev/api/films/';

fetch(url)
    .then((response) => response.json())
    .then((data) => {
        data.results.forEach((films) => {
            
            const bodyDiv = document.createElement('div')
            document.body.append(bodyDiv)
            const title = document.createElement('h3')
            const episode = document.createElement('h2')
            const shortInfo = document.createElement('p')
            const characters = document.createElement('div')
    
            title.append(`${films.title}`)
            episode.append('Episode ' + `${films.episode_id}`)
            shortInfo.append(`${films.opening_crawl}`)
            bodyDiv.append(episode, title, shortInfo)
            
            films.characters.forEach((character) => {
                const charactersP = document.createElement("p");
                fetch(character)
                    .then((response) => response.json())
                    .then(data => {
                        characters.append(data.name)
                        characters.append(charactersP)
                    })
            })
            bodyDiv.append(characters)
        })
    })
