'use strict'

const button = document.createElement("button");
button.innerHTML = "Вычислить по IP";
document.body.append(button);

button.onclick = async function() {
    const ip = await fetch("https://api.ipify.org/?format=json");
    const stringIp = await ip.json();

    const geo = await fetch(`http://ip-api.com/json/${stringIp.ip}?lang=ru&fields=continent,country,region,city,district`);
    const stringGeo = await geo.json();


    const continent = document.createElement("p");
    continent.innerHTML = stringGeo.continent;

    const country = document.createElement("p");
    country.innerHTML = stringGeo.country;

    const region = document.createElement("p");
    region.innerHTML = stringGeo.region;

    const city = document.createElement("p");
    city.innerHTML = stringGeo.city;

    const district = document.createElement("p");
    district.innerHTML = stringGeo.district;

    document.body.append(continent, country, region, city, district)
}



// const btn = document.createElement("button")
// btn.innerHTML = "Вычислить по IP"
// document.body.append(btn)

// btn.onclick = async function() {
//     const ip = await fetch("https://api.ipify.org?format=json");
//     const clearIp = await ip.json();

//     const geo = await fetch(`http://ip-api.com/json/${clearIp.ip}?lang=ru&fields=continent,country,region,city,district`);
//     const clearGeo = await geo.json();
//     console.log(clearGeo.continent)
    
//     const continent = document.createElement("p")
//     continent.innerHTML = clearGeo.continent
    
//     const country = document.createElement("p")
//     country.innerHTML = clearGeo.country
    
//     const region = document.createElement("p")
//     region.innerHTML = clearGeo.region
    
//     const city = document.createElement("p")
//     city.innerHTML = clearGeo.city
    
//     const district = document.createElement("p")
//     district.innerHTML = clearGeo.district
    
//     document.body.append(continent, country, region, city, district)
// }


// let button = document.getElementById("button")
// console.log(button);

// button.addEventListener("click", async function() {
//     let response = await fetch("https://api.ipify.org/?format=json");
//     let user = await response.json();

//     let servResponse = await fetch
// })